package moe.kanon.gitlab.release

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.coroutines.awaitStringResponseResult
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import moe.kanon.gitlab.release.models.*
import moe.kanon.kextensions.io.name
import moe.kanon.kextensions.io.not
import java.net.URLEncoder
import java.nio.file.Path

val gson = Gson()
val token = System.getenv("PRIVATE_TOKEN")!!
val projectUrl = System.getenv("CI_PROJECT_URL")!!
val projectId = System.getenv("CI_PROJECT_ID")!!
var currentTag = System.getenv("CI_BUILD_TAG")!!
val apiPoint = "https://gitlab.com/api/v4/projects/$projectId"

fun encode(url: String) = URLEncoder.encode(url, "UTF-8")!!

object Tags {
    
    fun getOrNull(tag: String = currentTag): TagModel? {
        var model: TagModel? = null
        
        runBlocking {
            Fuel.get("/repository/tags/${encode(tag)}").awaitStringResponseResult().third
                .fold(
                    { data -> model = gson.fromJson(data, TagModel::class.java) },
                    { error -> throw error.exception }
                )
        }
        
        return model
    }
    
    fun delete(tag: String = currentTag) {
        val (_, _, result) = Fuel.delete("/releases/${encode(tag)}").responseString()
        
        result.fold(
            { println("Successfully deleted tag \"$tag\".") },
            { error -> throw error.exception }
        )
    }
}

object Releases {
    
    fun create(tag: String = currentTag, name: String, description: String, links: List<Pair<String, String>>) {
        val model = PostReleaseModel(
            name,
            tag,
            description,
            PostAssetsModels(links.map { (name, url) -> PostLinkModel(name, url) })
        )
        
        val body = gson.toJson(model)!!
        
        val (_, _, result) = Fuel.post("/releases/")
            .header("Content-Type" to "application/json")
            .body(body)
            .responseString()
        
        result.fold(
            {
                println()
                println("Successfully created the new release: \"$name\". ($projectUrl @ $tag)")
                println("Link to new releases: $projectUrl/releases")
                println()
            },
            { error -> throw error.exception }
        )
    }
    
    fun delete(tag: String = currentTag) {
        val (_, _, result) = Fuel.delete("/releases/${encode(tag)}").responseString()
        
        result.fold(
            {
                val data = gson.fromJson(it, ReleaseModel::class.java)
    
                println()
                println("Successfully deleted release \"${data.name}\" for tag \"${data.tagName}\".")
                println()
            },
            { error -> throw error.exception }
        )
    }
    
    fun getOrNull(tag: String): ReleaseModel? {
        var model: ReleaseModel? = null
        
        runBlocking {
            Fuel.get("/releases/${encode(tag)}").awaitStringResponseResult().third
                .fold(
                    { data -> model = gson.fromJson(data, ReleaseModel::class.java) },
                    { error -> throw error.exception }
                )
        }
        
        return model
    }
    
    object Links {
        
        fun create(tag: String = currentTag, name: String, url: String) {
            val model = PostLinkModel(name, url)
            val body = gson.toJson(model)
            
            println("Attempting to append an asset link for \"$name\" to release \"$tag\"...")
            
            val (_, _, result) = Fuel.post("/releases/${encode(tag)}/assets/links")
                .header("Content-Type" to "application/json")
                .body(body)
                .responseString()
            
            result.fold(
                { println("\"$name\" has been added as an asset link for \"$tag\".") },
                { error -> throw error.exception }
            )
        }
        
        fun delete(tag: String = currentTag, id: Int) {
            val (_, _, result) = Fuel.delete("/releases/${encode(tag)}/assets/links/$id").responseString()
            
            result.fold(
                { println("Successfully deleted asset link \"$id\" for relase \"$tag\".") },
                { error -> throw error.exception }
            )
        }
    }
}

object Uploads {
    
    fun upload(file: Path): UploadedFileModel? {
        var responseModel: UploadedFileModel? = null
        
        println("Attempting to upload \"${file.name}\" to $projectUrl...")
        
        val (_, _, result) = Fuel.upload("/uploads")
            .add { FileDataPart(!file, name = "file") }
            .responseString()
        
        result.fold(
            { data ->
                val model = gson.fromJson(data, FileModel::class.java)
                
                responseModel = UploadedFileModel("$projectUrl${model.url}", file.name)
                
                println("Upload of \"${file.name}\" was successful. (${responseModel?.url})")
            },
            { error ->
                throw error.exception
            }
        )
        
        return responseModel
    }
}