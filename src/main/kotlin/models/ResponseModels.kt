package moe.kanon.gitlab.release.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class ReleaseModel(
    @SerializedName("tag_name") val tagName: String,
    val description: String,
    val name: String,
    @SerializedName("description_html") val descriptionHtml: String,
    @SerializedName("created_at") val createdAt: String,
    val author: AuthorModel,
    val commit: CommitModel,
    val assets: AssetsModel
) {
    class Deserializer : ResponseDeserializable<Array<ReleaseModel>> {
        override fun deserialize(content: String): Array<ReleaseModel> =
            Gson().fromJson(content, Array<ReleaseModel>::class.java)
    }
}

data class CommitModel(
    val id: String,
    @SerializedName("short_id") val shortId: String,
    val title: String,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("parent_ids") val parentIds: List<Any>,
    val message: String,
    @SerializedName("author_name") val authorName: String,
    @SerializedName("author_email") val authorEmail: String,
    @SerializedName("authored_date") val authoredDate: String,
    @SerializedName("committer_name") val committerName: String,
    @SerializedName("committer_email") val committerEmail: String,
    @SerializedName("committed_date") val committedDate: String
) {
    class Deserializer : ResponseDeserializable<Array<CommitModel>> {
        override fun deserialize(content: String): Array<CommitModel> =
            Gson().fromJson(content, Array<CommitModel>::class.java)
    }
}

data class AuthorModel(
    val id: Int,
    val name: String,
    val username: String,
    val state: String,
    @SerializedName("avatar_url") val avatarUrl: String,
    @SerializedName("web_url") val webUrl: String
) {
    class Deserializer : ResponseDeserializable<Array<AuthorModel>> {
        override fun deserialize(content: String): Array<AuthorModel> =
            Gson().fromJson(content, Array<AuthorModel>::class.java)
    }
}

data class AssetsModel(
    val count: Int,
    val sources: List<SourceModel>,
    val links: List<ReleaseLinkModel>
) {
    class Deserializer : ResponseDeserializable<Array<AssetsModel>> {
        override fun deserialize(content: String): Array<AssetsModel> =
            Gson().fromJson(content, Array<AssetsModel>::class.java)
    }
}

data class SourceModel(
    val format: String,
    val url: String
) {
    class Deserializer : ResponseDeserializable<Array<SourceModel>> {
        override fun deserialize(content: String): Array<SourceModel> =
            Gson().fromJson(content, Array<SourceModel>::class.java)
    }
}

data class ReleaseLinkModel(
    val format: String?,
    val id: Int,
    val name: String,
    val url: String,
    @SerializedName("external") val isExternal: Boolean
) {
    class Deserializer : ResponseDeserializable<Array<ReleaseLinkModel>> {
        override fun deserialize(content: String): Array<ReleaseLinkModel> =
            Gson().fromJson(content, Array<ReleaseLinkModel>::class.java)
    }
}

data class TagModel(
    val name: String,
    val message: String?,
    val target: String,
    val release: TagReleaseModel?
) {
    class Deserializer : ResponseDeserializable<Array<TagModel>> {
        override fun deserialize(content: String): Array<TagModel> =
            Gson().fromJson(content, Array<TagModel>::class.java)
    }
}

data class TagReleaseModel(
    @SerializedName("tag_name") val tagName: String,
    val description: String
) {
    class Deserializer : ResponseDeserializable<Array<TagReleaseModel>> {
        override fun deserialize(content: String): Array<TagReleaseModel> =
            Gson().fromJson(content, Array<TagReleaseModel>::class.java)
    }
}

data class FileModel(
    val alt: String,
    val url: String,
    val markdown: String
) {
    class Deserializer : ResponseDeserializable<Array<FileModel>> {
        override fun deserialize(content: String): Array<FileModel> =
            Gson().fromJson(content, Array<FileModel>::class.java)
    }
}

data class UploadedFileModel(
    val url: String,
    val name: String
)