package moe.kanon.gitlab.release.models

data class UpdateReleaseModel(val name: String?, val description: String?)