package moe.kanon.gitlab.release.models
import com.google.gson.annotations.SerializedName

data class PostReleaseModel(
    val name: String,
    @SerializedName("tag_name") val tagName: String,
    val description: String,
    val assets: PostAssetsModels
)

data class PostAssetsModels(
    val links: List<PostLinkModel>
)

data class PostLinkModel(
    val name: String,
    val url: String
)