package moe.kanon.gitlab.release

import com.github.kittinunf.fuel.core.FuelManager
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import moe.kanon.gitlab.release.models.UploadedFileModel
import moe.kanon.kextensions.io.KPath
import moe.kanon.kextensions.io.entries
import moe.kanon.kextensions.io.isDirectory
import kotlin.streams.asSequence
import kotlin.system.exitProcess

object Entry {
    
    @JvmStatic
    fun main(args: Array<String>) {
        FuelManager.instance.apply {
            basePath = apiPoint
            baseHeaders = mapOf("PRIVATE-TOKEN" to token)
        }
        
        //System.out.println(Tags.getOrNull("tagWithContent"))
        
        //Releases.create("test1", "test release", "release description", emptyList())
        
        //Releases.Links.create("test1", "epic", "https://www.kanon.moe")
        
        ArgParser(args).parseInto(::Arguments).run {
            currentTag = tag
    
            println()
            println("Current tag is set to \"$currentTag\".")
            println()
            
            val dir = KPath(directory)
            
            if (!dir.isDirectory) {
                println("-d/--directory needs to be a directory!")
                exitProcess(0)
            }
            
            val uploadedFiles = mutableListOf<UploadedFileModel>()
            
            dir.entries.asSequence().forEach {
                uploadedFiles += Uploads.upload(it)!!
                Thread.sleep(1000)
            }
            
            uploadedFiles.forEach {
                Releases.Links.create(name = it.name, url= it.url)
                Thread.sleep(1000)
            }
        }
    }
}

class Arguments(parser: ArgParser) {
    
    val tag by parser.storing(
        "-t",
        "--tag",
        help = "The tag to work on. (this is set to the 'CI_BUILD_TAG' by default.)"
    ).default(currentTag)
    
    val directory by parser.storing(
        "-d",
        "--directory",
        help = "All the files inside of the specified directory will be uploaded and appended to the tag."
    )
    
}
